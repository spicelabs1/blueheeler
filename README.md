# Blue Heeler

Record an irrefutable inventory of
[OmniBOR](https://omnibor.dev) identifiers
as well as other information relating to inventory
and deployment of software artifacts.
